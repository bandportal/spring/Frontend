import axios from "axios";
import {BASE_REST_API_URL} from "./config.js";

const REST_API_URL = BASE_REST_API_URL + "/event";
// const REST_API_URL_UPCOMING = BASE_REST_API_URL + "/event/upcoming";

export const listEvents = () => axios.get(REST_API_URL);

export const listUpcomingEvents = () => axios.get(REST_API_URL + '/upcoming');

export const createEvent = (e) => axios.post(REST_API_URL, castToEvent(e));

export const updateEvent = (e) => axios.put(REST_API_URL + '/' + e.eventId, castToEvent(e));

export const deleteEvent = (eventId) => axios.delete(REST_API_URL + '/' + eventId);

const castToEvent = (event) => {
    return {
        eventName: event.eventName,
        eventStart: event.eventStart,
        eventEnd: event.eventEnd === "" ? null : event.eventEnd,
        eventWebsite: event.eventWebsite === "" ? null : event.eventWebsite,

        placeId: event.place.placeId,
        bands: event.bands.map((b) => Number(b.bandId))
    }
}